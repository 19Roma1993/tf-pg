import { Component, OnInit } from '@angular/core';

import * as tf from '@tensorflow/tfjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    // const shape = [2, 3, -1];
    // const tensor = tf.tensor([
    //   1.0, 2.0, 3.0, 10.0, 20.0, 30.0, 1.0, 2.0, 3.0, 10.0, 20.0, 30.0, 1.0, 2.0, 3.0, 10.0, 20.0, 30.0
    // ]).reshape(shape);
    // tensor.print(true);
    // const variable = tf.variable(tensor);
    // variable.reshape([-1, 2, 3]).print(true);
    // variable.assign(tf.zeros([2, 3, 3]));
    // variable.print(true);
  }

}
