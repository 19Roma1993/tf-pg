import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { debounceTime, exhaustMap, mergeMap, tap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';

import * as tf from '@tensorflow/tfjs';
import { Tensor, History, TensorContainer, Sequential } from '@tensorflow/tfjs';
import { Layer } from '@tensorflow/tfjs-layers/dist/engine/topology';
import { Logs } from '@tensorflow/tfjs-layers/dist/logs';
import { CSVDataset, Dataset } from '@tensorflow/tfjs-data';

import * as tfvis from '@tensorflow/tfjs-vis';
import { Drawable, HistogramStats } from '@tensorflow/tfjs-vis/src/types';
import { tensorStats } from '@tensorflow/tfjs-vis/dist/util/math';
import { Visor } from '@tensorflow/tfjs-vis/dist/visor';

import { SERVER_API_URL } from '../app.constants';
import { HousingEnum, ModelTypesEnum } from '../typings/enums';
import { DataService } from '../shared/services/data.service';
import { showHistory, showTable } from '../shared/utils/tfvis-utils';
import { separateDataByRanges } from '../shared/utils/tf-utils';

@Component({
  selector: 'app-california-housing',
  templateUrl: './california-housing.component.html',
  styleUrls: ['./california-housing.component.sass']
})
export class CaliforniaHousingComponent implements OnInit {
  visor: Visor;
  modelTypesEnum = ModelTypesEnum;
  form: FormGroup;
  getDatasetSubject = new Subject();
  datasetRange: { [key: string]: number };

  get modelType(): ModelTypesEnum {
    return this.form.get('modelType').value;
  }

  constructor(
    private fb: FormBuilder,
    private dataService: DataService
  ) {
  }

  ngOnInit() {
    this.buildDataRangesForm();
    this.visor = this.initTfVisor();
    this.initDatasetSubject();
  }

  buildDataRangesForm() {
    this.form = this.fb.group({
      modelType: [ModelTypesEnum.LINEAR]
    });
  }

  setRange(range: { [key: string]: number }) {
    this.datasetRange = range;
  }

  initDatasetSubject() {
    this.getDatasetSubject.pipe(
      // debounceTime(1000),
      // distinctUntilChanged(),
      exhaustMap(() => this.dataService.getCaliforniaHousingDataset()),
      mergeMap((res: TensorContainer[]) => fromPromise(this.createModel(res, this.datasetRange))),
      tap((res: any) => {
        console.log('res', res);
      })
    ).subscribe();
  }

  getDataset() {
    if (!this.visor.isFullscreen()) {
      this.visor.toggleFullScreen();
    }
    this.getDatasetSubject.next();
  }

  initTfVisor(): Visor {
    return tfvis.visor();
  }

  private makeMapFromArray(arr: string[]) {
    return Object.keys(arr).reduce((res, cur, index) => {
      res[cur] = index;
      return res;
    }, {});
  }

  private getTensorColumn(tensor: Tensor, columnIndex: number): Tensor {
    return tensor.slice([0, columnIndex], [tensor.shape[0], 1]);
  }

  private getTensorRow(tensor: Tensor, rowIndex: number): Tensor {
    return tensor.slice([rowIndex, 0], [1, tensor.shape[1]]);
  }

  private makeBinaryVector(value: number, minValue: number, maxValue: number): number[] {
    return Array.from(new Array(Math.ceil(maxValue) - Math.floor(minValue)), (v: any, index: number) => {
      return Math.floor(value) - Math.floor(minValue) === index ? 1 : 0;
    });
  }

  private async showTensorValuesDistribution(tensor: Tensor, tab: string, name: string) {
    const surface: Drawable = this.visor.surface({
      name: `${name.toUpperCase()}`,
      tab
    });
    await tfvis.show.valuesDistribution(surface, tensor);
  }

  private showCallbacks(callbacks?: string[]) {
    const options = callbacks ? {callbacks} : {};
    const surface: Drawable = this.visor.surface({
      name: 'Callbacks',
      tab: 'Train'
    });
    return tfvis.show.fitCallbacks(surface, ['loss'], options);
  }

  private async showModel(model: Sequential) {
    const modelSummarySurface: Drawable = this.visor.surface({
      name: 'Summary',
      tab: 'Model'
    });
    await tfvis.show.modelSummary(modelSummarySurface, model);

    model.layers.forEach(async (layer: Layer, index: number) => {
      const layerSurface: Drawable = this.visor.surface({
        name: `Layer ${index}`,
        tab: 'Model'
      });
      await tfvis.show.layer(layerSurface, layer);
    });
  }

  private showCustomTable(headers: string[], values: any[][], tab: string, name: string) {
    const surface: Drawable = this.visor.surface({
      name,
      tab
    });
    tfvis.render.table(surface, {
      headers,
      values
    });
  }

  private* processDeviantDatasetValues(
    datasetSize: number,
    featuresMap: { [key: string]: number },
    labelsMap: { [key: string]: number }
  ) {
    console.log('processDeviantDatasetValues datasetSize', datasetSize);
    let value: { [key: string]: any[] };

    const tableHeader = ['#'].concat(Object.keys(featuresMap)).concat(Object.keys(labelsMap));
    const roomsPerPersonTableData = [];

    for (let i = 0; i < datasetSize; i++) {
      value = yield value;

      if (value.xs[HousingEnum.ROOMS_PER_PERSON] >= 5) {
        roomsPerPersonTableData.push(Object.values(value.xs).concat(Object.values(value.ys)));
      }
    }

    this.showCustomTable(
      tableHeader,
      roomsPerPersonTableData
        .sort((a: any[], b: any[]) => b[featuresMap[HousingEnum.ROOMS_PER_PERSON]] - a[featuresMap[HousingEnum.ROOMS_PER_PERSON]])
        .map((item: any[], index: number) => [`<strong>${index + 1}</strong>`].concat(item)),
      'Deviant Values',
      HousingEnum.ROOMS_PER_PERSON.toUpperCase() + ' ≥ 5'
    );

    return;
  }

  private async processAllDatasetValues(
    csvDataset: Dataset<any>,
    featuresMap: { [key: string]: number },
    labelsMap: { [key: string]: number }
  ) {
    const dataset: Dataset<any> = csvDataset
      .mapAsync(async (value: { [key: string]: any }) => ({
        xs: Object.values(value.xs),
        ys: Object.values(value.ys)
      }))
      .batch(csvDataset.size);
    const datasetValue: { [key: string]: Tensor } = (await (await dataset.iterator()).next()).value;

    const valuesAnalysis: { [key: string]: HistogramStats } = {};

    if (typeof featuresMap[HousingEnum.LONGITUDE] === 'number') {
      const longitudeT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.LONGITUDE]);
      await this.showTensorValuesDistribution(longitudeT, 'Values Distribution', HousingEnum.LONGITUDE);
      // console.log('longitudeT', longitudeT);
      // longitudeT.print(true);
      valuesAnalysis[HousingEnum.LONGITUDE] = await tensorStats(longitudeT);
    }

    if (typeof featuresMap[HousingEnum.LATITUDE] === 'number') {
      const latitudeT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.LATITUDE]);
      await this.showTensorValuesDistribution(latitudeT, 'Values Distribution', HousingEnum.LATITUDE);
      // console.log('latitudeT', latitudeT);
      // latitudeT.print(true);
      valuesAnalysis[HousingEnum.LATITUDE] = await tensorStats(latitudeT);
    }

    if (typeof featuresMap[HousingEnum.HOUSING_MEDIAN_AGE] === 'number') {
      const housingMedianAgeT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.HOUSING_MEDIAN_AGE]);
      await this.showTensorValuesDistribution(housingMedianAgeT, 'Values Distribution', HousingEnum.HOUSING_MEDIAN_AGE);
      // console.log('housingMedianAgeT', housingMedianAgeT);
      // housingMedianAgeT.print(true);
      valuesAnalysis[HousingEnum.HOUSING_MEDIAN_AGE] = await tensorStats(housingMedianAgeT);
    }

    if (typeof featuresMap[HousingEnum.TOTAL_ROOMS] === 'number') {
      const totalRoomsT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.TOTAL_ROOMS]);
      await this.showTensorValuesDistribution(totalRoomsT, 'Values Distribution', HousingEnum.TOTAL_ROOMS);
      // console.log('totalRoomsT', totalRoomsT);
      // totalRoomsT.print(true);
      valuesAnalysis[HousingEnum.TOTAL_ROOMS] = await tensorStats(totalRoomsT);
    }

    if (typeof featuresMap[HousingEnum.TOTAL_BEDROOMS] === 'number') {
      const totalBedroomsT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.TOTAL_BEDROOMS]);
      await this.showTensorValuesDistribution(totalBedroomsT, 'Values Distribution', HousingEnum.TOTAL_BEDROOMS);
      // console.log('totalBedroomsT', totalBedroomsT);
      // totalBedroomsT.print(true);
      valuesAnalysis[HousingEnum.TOTAL_BEDROOMS] = await tensorStats(totalBedroomsT);
    }

    if (typeof featuresMap[HousingEnum.POPULATION] === 'number') {
      const populationT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.POPULATION]);
      await this.showTensorValuesDistribution(populationT, 'Values Distribution', HousingEnum.POPULATION);
      // console.log('populationT', populationT);
      // populationT.print(true);
      valuesAnalysis[HousingEnum.POPULATION] = await tensorStats(populationT);
    }

    if (typeof featuresMap[HousingEnum.HOUSEHOLDS] === 'number') {
      const householdsT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.HOUSEHOLDS]);
      await this.showTensorValuesDistribution(householdsT, 'Values Distribution', HousingEnum.HOUSEHOLDS);
      // console.log('householdsT', householdsT);
      // householdsT.print(true);
      valuesAnalysis[HousingEnum.HOUSEHOLDS] = await tensorStats(householdsT);
    }

    if (typeof featuresMap[HousingEnum.MEDIAN_INCOME] === 'number') {
      const medianIncomeT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.MEDIAN_INCOME]);
      await this.showTensorValuesDistribution(medianIncomeT, 'Values Distribution', HousingEnum.MEDIAN_INCOME);
      // console.log('medianIncomeT', medianIncomeT);
      // medianIncomeT.print(true);
      valuesAnalysis[HousingEnum.MEDIAN_INCOME] = await tensorStats(medianIncomeT);
    }

    if (typeof featuresMap[HousingEnum.ROOMS_PER_PERSON] === 'number') {
      const roomsPerPersonT = this.getTensorColumn(datasetValue.xs, featuresMap[HousingEnum.ROOMS_PER_PERSON]);
      await this.showTensorValuesDistribution(roomsPerPersonT, 'Values Distribution', HousingEnum.ROOMS_PER_PERSON);
      // console.log('roomsPerPersonT', roomsPerPersonT);
      // roomsPerPersonT.print(true);
      valuesAnalysis[HousingEnum.ROOMS_PER_PERSON] = await tensorStats(roomsPerPersonT);
    }

    if (typeof labelsMap[HousingEnum.MEDIAN_HOUSE_VALUE] === 'number') {
      const medianHouseValueT = this.getTensorColumn(datasetValue.ys, labelsMap[HousingEnum.MEDIAN_HOUSE_VALUE]);
      await this.showTensorValuesDistribution(medianHouseValueT, 'Values Distribution', HousingEnum.MEDIAN_HOUSE_VALUE);
      // console.log('medianHouseValueT', medianHouseValueT);
      // medianHouseValueT.print(true);
      valuesAnalysis[HousingEnum.MEDIAN_HOUSE_VALUE] = await tensorStats(medianHouseValueT);
    }

    return valuesAnalysis;
  }

  private async initDatasetValuesAnalysis(originalCsvDatasetArray: TensorContainer[]) {
    const originalCsvDataset = tf.data.array(originalCsvDatasetArray);
    console.log('originalCsvDataset iterator summary', (await originalCsvDataset.iterator()).summary());

    const firstOriginalCsvDatasetValue: any = (await (await originalCsvDataset.iterator()).next()).value;

    const dataFeaturesMap: { [key: string]: number } = this.makeMapFromArray(firstOriginalCsvDatasetValue.xs);
    const dataLabelsMap: { [key: string]: number } = this.makeMapFromArray(firstOriginalCsvDatasetValue.ys);

    console.log('dataFeaturesMap', dataFeaturesMap);
    console.log('dataLabelsMap', dataLabelsMap);

    const processDeviantDatasetValues = this.processDeviantDatasetValues(originalCsvDatasetArray.length, dataFeaturesMap, dataLabelsMap);
    processDeviantDatasetValues.next(firstOriginalCsvDatasetValue);

    await originalCsvDataset.forEachAsync((value: { [key: string]: any }) => {
      processDeviantDatasetValues.next(value);
    });

    console.log('processDeviantDatasetValues', processDeviantDatasetValues); // doesn't work return

    return await this.processAllDatasetValues(originalCsvDataset, dataFeaturesMap, dataLabelsMap);
  }

  private async getDatasetFeaturesAndLabels(dataset: Dataset<any>, prefix: string) {
    const datasetValue = (await (await dataset.iterator()).next()).value;

    const datasetFeatures: Tensor = datasetValue.xs;
    console.log(prefix, 'datasetFeatures', datasetFeatures);
    // const datasetFeaturesArray = await datasetFeatures.array();
    // console.log('datasetFeaturesArray', datasetFeaturesArray);
    datasetFeatures.print(true);

    const datasetLabels: Tensor = datasetValue.ys;
    console.log(prefix, 'datasetLabels', datasetLabels);
    datasetLabels.print(true);

    return {datasetFeatures, datasetLabels};
  }

  private processDataset(datasetArray: TensorContainer[], valuesAnalysis: { [key: string]: HistogramStats }) {
    return tf.data.array(datasetArray)
      .mapAsync(async (value: { [key: string]: any }) => {
        // value.xs[HousingEnum.LONGITUDE] = this.makeBinaryVector(
        //   value.xs[HousingEnum.LONGITUDE],
        //   valuesAnalysis[HousingEnum.LONGITUDE].min,
        //   valuesAnalysis[HousingEnum.LONGITUDE].max
        // );
        // value.xs[HousingEnum.LATITUDE] = this.makeBinaryVector(
        //   value.xs[HousingEnum.LATITUDE],
        //   valuesAnalysis[HousingEnum.LATITUDE].min,
        //   valuesAnalysis[HousingEnum.LATITUDE].max
        // );
        // console.log('Object.values(value.xs)', Object.values(value.xs));
        if (this.modelType === ModelTypesEnum.LOGISTIC) {
          value.ys[HousingEnum.MEDIAN_HOUSE_VALUE] = value.ys[HousingEnum.MEDIAN_HOUSE_VALUE] > 265 ? 1 : 0;
        }
        return {
          xs: Object.values(value.xs),
          ys: Object.values(value.ys)
        };
      })
      .batch(datasetArray.length)
      ;
  }

  private async createModel(originalCsvDatasetArray: TensorContainer[], dataRanges: { [key: string]: number }) {
    console.log('tf.memory() before', tf.memory());
    showTable(tf.memory(), 'Memory', 'Before');

    const valuesAnalysis = await this.initDatasetValuesAnalysis(originalCsvDatasetArray);
    console.log('valuesAnalysis', valuesAnalysis);

    const {trainData, validateData, testData} = separateDataByRanges(originalCsvDatasetArray, dataRanges);

    const trainDataset: Dataset<any> = this.processDataset(trainData, valuesAnalysis);

    const validateDataset: Dataset<any> = this.processDataset(validateData, valuesAnalysis);

    const testDataset: Dataset<any> = this.processDataset(testData, valuesAnalysis);

    const {datasetFeatures: trainDatasetFeatures, datasetLabels: trainDatasetLabels} =
      await this.getDatasetFeaturesAndLabels(trainDataset, 'train');

    const {datasetFeatures: validateDatasetFeatures, datasetLabels: validateDatasetLabels} =
      await this.getDatasetFeaturesAndLabels(validateDataset, 'validate');

    const {datasetFeatures: testDatasetFeatures, datasetLabels: testDatasetLabels} =
      await this.getDatasetFeaturesAndLabels(testDataset, 'test');

    const inputShape = [(await (await trainDataset.iterator()).next()).value.xs.shape.pop()];
    console.log('inputShape', inputShape);

    const model: Sequential = tf.sequential();
    model.add(
      tf.layers.dense({
        inputShape,
        units: 1,
        kernelRegularizer: tf.regularizers.l2({l2: 0.01})
      })
    );
    model.summary();

    model.weights.forEach(w => {
      console.log('weight.name, weight.shape |', w.name, w.shape);
    });
    model.compile({
      optimizer: tf.train.sgd(0.00000006),
      loss: this.modelType === ModelTypesEnum.LINEAR ? tf.losses.meanSquaredError : tf.losses.logLoss
    });

    const info: History = await model.fitDataset(trainDataset, {
      epochs: 50,
      callbacks: [
        {
          onEpochEnd: (epoch: number, logs: Logs) => {
            console.log('onEpochEnd: ', 'epoch', epoch, 'logs', logs);
          }
        },
        this.showCallbacks(['onEpochEnd'])
      ]
    });
    console.log('fitDataset info', info);

    await showHistory(info);

    const weights = model.getWeights();
    console.log('weights', weights);
    weights.forEach((item: Tensor) => {
      item.print(true);
    });

    console.log('model after training', model);
    model.summary();
    await this.showModel(model);

    const validateModel = await this.predictModel(model, validateDatasetFeatures, validateDatasetLabels, valuesAnalysis, 'validate');
    console.log('validateModel', validateModel);

    const testModel = await this.predictModel(model, testDatasetFeatures, testDatasetLabels, valuesAnalysis, 'test');
    console.log('testModel', testModel);

    console.log('tf.memory() after', tf.memory());
    showTable(tf.memory(), 'Memory', 'After');

    return {
      validateModel,
      testModel
    };
  }

  private async predictModel(
    trainedModel: Sequential,
    featuresToPredict: Tensor,
    labelsToCompare: Tensor,
    valuesAnalysis,
    logPrefix: string = '') {
    console.log('----------', logPrefix, 'predictModel', '----------');

    const prediction = trainedModel.predict(featuresToPredict) as Tensor;
    console.log(logPrefix, 'prediction', prediction);
    prediction.print(true);
    await this.showTensorValuesDistribution(prediction, logPrefix, 'Prediction');

    if (this.modelType === ModelTypesEnum.LINEAR) {
      const meanSquaredError = tf.metrics.meanSquaredError(labelsToCompare, prediction);
      const rootMeanSquaredError = tf.sqrt(meanSquaredError);
      console.log(logPrefix, 'rootMeanSquaredError', rootMeanSquaredError);
      rootMeanSquaredError.print(true);

      const minLabel = valuesAnalysis[HousingEnum.MEDIAN_HOUSE_VALUE].min;
      const maxLabel = valuesAnalysis[HousingEnum.MEDIAN_HOUSE_VALUE].max;
      const labelDiff = maxLabel - minLabel;
      console.log(logPrefix, 'labelDiff', labelDiff);

      const isErrorLess = tf.less(rootMeanSquaredError, labelDiff);
      console.log(logPrefix, 'isErrorLess', isErrorLess);
      isErrorLess.print(true);

      return {
        prediction,
        rootMeanSquaredError,
        labelDiff,
        isErrorLess
      };
    } else {

      return {
        prediction
      };
    }
  }
}
