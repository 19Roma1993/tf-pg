import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaliforniaHousingComponent } from './california-housing.component';

describe('CaliforniaHousingComponent', () => {
  let component: CaliforniaHousingComponent;
  let fixture: ComponentFixture<CaliforniaHousingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaliforniaHousingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaliforniaHousingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
