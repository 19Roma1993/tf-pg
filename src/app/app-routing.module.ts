import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaliforniaHousingComponent } from './california-housing/california-housing.component';
import { CarsComponent } from './cars/cars.component';
import { DigitComponent } from './digit/digit.component';

const routes: Routes = [
  {
    path: 'california-housing',
    component: CaliforniaHousingComponent
  },
  {
    path: 'cars',
    component: CarsComponent
  },
  {
    path: 'digit',
    component: DigitComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
