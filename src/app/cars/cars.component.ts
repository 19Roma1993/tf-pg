import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, exhaustMap, tap } from 'rxjs/operators';
import { History, Sequential, Tensor, Shape } from '@tensorflow/tfjs';
import * as tf from '@tensorflow/tfjs';
import * as tfvis from '@tensorflow/tfjs-vis';

import { DataService } from '../shared/services/data.service';
import * as tfvisUtils from '../shared/utils/tfvis-utils';
import * as tfUtils from '../shared/utils/tf-utils';
import { CarEnum } from '../typings/enums';
import { Car, NormalizationData } from '../typings/interfaces';
import { Point2D } from '@tensorflow/tfjs-vis/src/types';


@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.sass']
})
export class CarsComponent implements OnInit {
  datasetRange: { [key: string]: number };
  private getDatasetSubject = new Subject();

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.initDatasetSubject();
  }

  setRange(range: { [key: string]: number }) {
    this.datasetRange = range;
  }

  initDatasetSubject() {
    this.getDatasetSubject.pipe(
      debounceTime(1000),
      exhaustMap(() => this.dataService.getCarsDataset()),
      tap(async (res: Car[]) => {
        this.visualizeInitialData(res);
        const {trainData, validateData, testData} = tfUtils.separateDataByRanges(res, this.datasetRange);
        const {
          inputs,
          labels,
          inputMin,
          inputMax,
          labelMin,
          labelMax
        } = this.prepareModelData(trainData);
        const model = this.createModel(inputs.shape);
        await tfvisUtils.showModelSummary(model, 'Model');
        await this.trainModel(model, inputs, labels);
        const {feature: validateFeature, label: validateLabel} = this.predictModel(model, validateData, {
          inputMin,
          inputMax,
          labelMin,
          labelMax
        });
        this.visualizePredictionData(trainData, validateData, validateFeature, validateLabel);
      })
    ).subscribe();
  }

  getDataset() {
    tfvisUtils.showVisor();
    this.getDatasetSubject.next();
  }

  private async visualizeInitialData(data: Car[]) {
    await tfvisUtils.showScatterPlot(
      'First Look',
      {values: tfvisUtils.make2dPointsFromObject<Car>(data, CarEnum.HORSEPOWER, CarEnum.MILES_PER_GALLON)},
      'Horsepower',
      'MPG'
    );
    await tfvisUtils.showScatterPlot(
      'First Look',
      {values: tfvisUtils.make2dPointsFromObject<Car>(data, CarEnum.DISPLACEMENT, CarEnum.MILES_PER_GALLON)},
      'Engine Displacement',
      'MPG'
    );
    await tfvisUtils.showScatterPlot(
      'First Look',
      {values: tfvisUtils.make2dPointsFromObject<Car>(data, CarEnum.WEIGHT_IN_LBS, CarEnum.MILES_PER_GALLON)},
      'Weight In LBS',
      'MPG'
    );
    await tfvisUtils.showScatterPlot(
      'First Look',
      {values: tfvisUtils.make2dPointsFromObject<Car>(data, CarEnum.ACCELERATION, CarEnum.MILES_PER_GALLON)},
      'Acceleration',
      'MPG'
    );
  }

  private async visualizePredictionData(trainData: Car[], validateData: Car[], predictFeature: number[], predictLabel: number[]) {
    await tfvisUtils.showScatterPlot(
      'Prediction',
      {
        values: [
          tfvisUtils.make2dPointsFromObject<Car>(trainData, CarEnum.HORSEPOWER, CarEnum.MILES_PER_GALLON) as Point2D[],
          tfvisUtils.make2dPointsFromArrays(predictFeature, predictLabel) as Point2D[]
        ],
        series: ['original', 'predicted']
      },
      '(original / predicted) Horsepower',
      'MPG'
    );
    await tfvisUtils.showScatterPlot(
      'Prediction',
      {
        values: [
          tfvisUtils.make2dPointsFromObject<Car>(validateData, CarEnum.HORSEPOWER, CarEnum.MILES_PER_GALLON) as Point2D[],
          tfvisUtils.make2dPointsFromArrays(predictFeature, predictLabel) as Point2D[]
        ],
        series: ['validation', 'predicted']
      },
      '(validation/ predicted) Horsepower',
      'MPG'
    );
  }

  private prepareModelData(data: Car[]) {
    return tf.tidy(() => {
      tf.util.shuffle(data);

      const {[CarEnum.HORSEPOWER]: inputs, [CarEnum.MILES_PER_GALLON]: labels} =
        tfUtils.separateData(data, [CarEnum.HORSEPOWER, CarEnum.MILES_PER_GALLON]);

      const inputsT = tf.tensor2d(inputs, [inputs.length, 1]);
      const labelsT = tf.tensor2d(labels, [labels.length, 1]);

      const {min: inputMin, max: inputMax, normalized: normalizedInputs} = tfUtils.normalizeTensor(inputsT);
      const {min: labelMin, max: labelMax, normalized: normalizedLabels} = tfUtils.normalizeTensor(labelsT);

      return {
        inputs: normalizedInputs,
        labels: normalizedLabels,
        inputMin,
        inputMax,
        labelMin,
        labelMax
      };
    });
  }

  private createModel(shape: Shape): Sequential {
    const model: Sequential = tf.sequential();
    model.add(tf.layers.dense({
      inputShape: [shape[1]],
      units: 1
    }));
    model.add(tf.layers.dense({units: 50, activation: 'sigmoid'}));
    // model.add(tf.layers.dense({units: 50, activation: 'sigmoid'}));
    model.add(tf.layers.dense({units: 1}));

    return model;
  }

  private async trainModel(model: Sequential, inputs: Tensor, labels: Tensor): Promise<History> {
    model.compile({
      optimizer: tf.train.adam(),
      loss: tf.losses.meanSquaredError,
      // loss: tf.losses.logLoss,
      metrics: [tf.metrics.meanSquaredError]
    });

    const batchSize = 32;
    const epochs = 50;

    return await model.fit(inputs, labels, {
      batchSize,
      epochs,
      shuffle: true,
      callbacks: tfvis.show.fitCallbacks(
        {name: 'Training Performance', tab: 'Training'},
        ['loss', 'mse'],
        {height: 200, callbacks: ['onEpochEnd']}
      )
    });
  }

  private predictModel(model: Sequential, data: Car[], normalizationData: NormalizationData) {
    return tf.tidy(() => {
      const {
        inputMin,
        inputMax,
        labelMin,
        labelMax
      } = normalizationData;
      const {[CarEnum.HORSEPOWER]: input} = tfUtils.separateData(data, [CarEnum.HORSEPOWER]);
      const inputT = tf.tensor2d(input, [input.length, 1]);
      const {normalized} = tfUtils.normalizeTensor(inputT, inputMin, inputMax);
      const normalizedPrediction = model.predict(normalized) as Tensor;
      const prediction = tfUtils.unNormalizeTensor(normalizedPrediction, labelMin, labelMax);
      return {
        feature: input,
        label: Array.from(prediction.dataSync())
      };
    });
  }

}
