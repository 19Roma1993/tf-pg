import { CarEnum } from './enums';
import { Tensor } from '@tensorflow/tfjs';

export interface Car {
  [CarEnum.NAME]?: string;
  [CarEnum.MILES_PER_GALLON]?: number;
  [CarEnum.CYLINDERS]?: number;
  [CarEnum.DISPLACEMENT]?: number;
  [CarEnum.HORSEPOWER]?: number;
  [CarEnum.WEIGHT_IN_LBS]?: number;
  [CarEnum.ACCELERATION]?: number;
  [CarEnum.YEAR]?: string;
  [CarEnum.ORIGIN]?: string;
}

export interface NormalizationData {
  inputMin: Tensor;
  inputMax: Tensor;
  labelMin: Tensor;
  labelMax: Tensor;
}
