import { Component, OnInit } from '@angular/core';
import { Tensor, Sequential, SymbolicTensor, History, Logs } from '@tensorflow/tfjs';
import * as tf from '@tensorflow/tfjs';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    const tensor: Tensor = tf.tensor([[1, 2], [3, 4]], [2, 2]);
    console.log('tensor', tensor);
    tensor.print(true);
    tensor.array().then((res) => {
      console.log('tensor.array()', res);
      console.log('here math operations really complete');
    });

    const anotherTensor = tensor.reshape([1, -1]);
    console.log('anotherTensor', anotherTensor);
    anotherTensor.print(true);
    anotherTensor.data().then((res) => {
      console.log('anotherTensor.data()', res);
      console.log('here math operations really complete');
    });

    console.log('memory', tf.memory());
    // tf.setBackend('cpu');
    console.log('tensorflow backend', tf.getBackend());
    console.log('on webgl backend you have to manually clean memory with dispose() or tidy()');

    const sequentialModel: Sequential = tf.sequential({
      layers: [
        tf.layers.dense({inputShape: [784], units: 32, activation: 'relu'}),
        tf.layers.dense({units: 10, activation: 'softmax'}),
      ],
      name: 'sequentialModel'
    });
    console.log('sequentialModel', sequentialModel);
    sequentialModel.summary();
    sequentialModel.compile({
      optimizer: 'sgd',
      loss: 'categoricalCrossentropy',
      metrics: ['accuracy']
    });

    // Create an arbitrary graph of layers, by connecting them
    // via the apply() method.
    const input = tf.input({shape: [784]});
    const dense1 = tf.layers.dense({units: 32, activation: 'relu'}).apply(input);
    const dense2 = tf.layers.dense({units: 10, activation: 'softmax'}).apply(dense1);
    const functionalModel = tf.model({inputs: input, outputs: dense2 as SymbolicTensor, name: 'functionalModel'});
    console.log('functionalModel', functionalModel);
    functionalModel.summary();

    const activationTensor = tf.tensor([-2, 1, 0, 5], [4]);
    console.log('activationTensor', activationTensor);
    activationTensor.print(true);
    const activationTensorRelu: Tensor = tf.layers.activation({activation: 'relu'}).apply(activationTensor) as Tensor;
    console.log('activationTensorRelu', activationTensorRelu);
    activationTensorRelu.print(true);

    const x = tf.input({shape: [32]});
    const y = tf.layers.dense({units: 3, activation: 'softmax'}).apply(x);
    const model = tf.model({inputs: x, outputs: y as SymbolicTensor});
    (model.predict(tf.ones([2, 32])) as Tensor).print(true);

    // Generate dummy data.
    const data = tf.randomNormal([100, 784]);
    const labels = tf.randomUniform([100, 10]);

    function onBatchEnd(batch: number, logs?: Logs) {
      console.log('logs', logs);
      console.log('Accuracy', logs.acc);
      return Promise.resolve();
    }

    // Train for 5 epochs with batch size of 32.
    sequentialModel.fit(data, labels, {
      epochs: 5,
      batchSize: 32,
      callbacks: {onBatchEnd}
    }).then((info: History) => {
      console.log('info', info);
      console.log('Final accuracy', info.history.acc);
      const prediction: Tensor = sequentialModel.predict(tf.randomNormal([3, 784])) as Tensor;
      console.log('prediction', prediction);
      prediction.print(true);
    });
  }

}
