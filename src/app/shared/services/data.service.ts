import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SERVER_API_URL} from '../../app.constants';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getCaliforniaHousingDataset() {
    return this.http.get(SERVER_API_URL + 'california-housing');
  }

  getCarsDataset() {
    return this.http.get(SERVER_API_URL + 'cars');
  }
}
