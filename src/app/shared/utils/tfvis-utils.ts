import { Drawable, Point2D, XYPlotData, XYPlotOptions } from '@tensorflow/tfjs-vis/src/types';
import { Visor } from '@tensorflow/tfjs-vis/dist/visor';
import * as tfvis from '@tensorflow/tfjs-vis';
import { LayersModel, History } from '@tensorflow/tfjs';

export function showVisor() {
  const visor: Visor = tfvis.visor();
  if (!visor.isOpen()) {
    visor.open();
  }
  if (!visor.isFullscreen()) {
    visor.toggleFullScreen();
  }
}

export function make2dPointsFromObject<T>(data: T[], x: keyof T, y: keyof T): Point2D[][]|Point2D[] {
  return data.map((item: T) => ({
    x: item[x] as unknown as number,
    y: item[y] as unknown as number
  }));
}

export function make2dPointsFromArrays(a: number[], b: number[]): Point2D[][]|Point2D[] {
  return a.map((value: number, index: number) => ({
    x: value,
    y: b[index]
  }));
}

export async function showScatterPlot(tab: string, data: XYPlotData, xLabel: string, yLabel: string) {
  const container: Drawable = {
    name: `${xLabel} v ${yLabel}`,
    tab
  };
  const opts: XYPlotOptions = {
    xLabel,
    yLabel,
    height: 200,
    width: 500,
    zoomToFit: true
  };
  await tfvis.render.scatterplot(container, data, opts);
}

export async function showModelSummary(model: LayersModel, tab: string) {
  await tfvis.show.modelSummary({
    name: 'Model Summary',
    tab
  }, model);
}



export function showTable(dataMap: object, tab: string, name: string, visor: Visor = tfvis.visor()) {
  const surface: Drawable = visor.surface({
    name,
    tab
  });
  tfvis.render.table(surface, {
    headers: Object.keys(dataMap),
    values: [Object.values(dataMap)]
  });
}

export async function showHistory(info: History, visor: Visor = tfvis.visor()) {
  const surface: Drawable = visor.surface({
    name: 'History',
    tab: 'Training'
  });
  await tfvis.show.history(surface, info as any, Object.keys(info.history));
}
