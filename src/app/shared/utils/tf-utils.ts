import { Tensor } from '@tensorflow/tfjs';
import { showTable } from './tfvis-utils';

export function separateData<T>(data: T[], properties?: (keyof T)[]): {[key: string]: any[]} {
  return data.reduce((result: {[key: string]: any[]}, current: T) => {
    (properties || (Object.keys(current) as (keyof T)[])).forEach((property: keyof T) => {
      if (result.hasOwnProperty(property)) {
        result[property as string].push(current[property]);
      } else {
        result[property as string] = [current[property]];
      }
    });
    return result;
  }, {});
}

export function separateDataByRanges(data: any[], ranges: { [key: string]: number }) {
  const dataLength = data.length;

  const dataSeparationMap = {
    train: Math.round(dataLength * ranges.train),
    validate: Math.round(dataLength * ranges.validate),
    test: Math.round(dataLength * ranges.test)
  };
  showTable(dataSeparationMap, 'Config', 'Data Separation Map');

  const trainData = data.slice(0, dataSeparationMap.train);
  const validateData = data.slice(
    dataSeparationMap.train,
    dataSeparationMap.train + dataSeparationMap.validate
  );
  const testData = data.slice(
    dataSeparationMap.train + dataSeparationMap.validate,
    dataSeparationMap.train + dataSeparationMap.validate + dataSeparationMap.test);

  console.log('trainData', trainData);
  console.log('validateData', validateData);
  console.log('testData', testData);

  return {trainData, validateData, testData};
}

export function normalizeTensor(tensor: Tensor, min?: Tensor, max?: Tensor) {
  min = min || tensor.min();
  max = max || tensor.max();
  const normalized = tensor.sub(min).div(max.sub(min));
  return {min, max, normalized};
}

export function unNormalizeTensor(normalized: Tensor, min: Tensor, max: Tensor) {
  return normalized.mul(max.sub(min)).add(min);
}
