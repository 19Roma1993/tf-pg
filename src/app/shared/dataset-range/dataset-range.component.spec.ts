import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetRangeComponent } from './dataset-range.component';

describe('DatasetRangeComponent', () => {
  let component: DatasetRangeComponent;
  let fixture: ComponentFixture<DatasetRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasetRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
