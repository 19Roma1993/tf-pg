import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-dataset-range',
  templateUrl: './dataset-range.component.html',
  styleUrls: ['./dataset-range.component.sass']
})
export class DatasetRangeComponent implements OnInit {
  form: FormGroup;

  @Output() range: EventEmitter<{ [key: string]: number }> = new EventEmitter();

  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.buildDataRangesForm();
    this.initDataRangesFormValueChanges();
    this.emitRange();
  }

  buildDataRangesForm() {
    this.form = this.fb.group({
      train: [0.7, [Validators.compose([Validators.required, Validators.min(0.01), Validators.max(0.98)])]],
      validate: [0.15, [Validators.compose([Validators.required, Validators.min(0.01), Validators.max(0.98)])]],
      test: [0.15, [Validators.compose([Validators.required, Validators.min(0.01), Validators.max(0.98)])]]
    });
  }

  initDataRangesFormValueChanges() {
    this.form.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(({train, validate, test}) => {
      const rangesSum: number = train + validate + test;
      if (rangesSum > 1) {
        const rest = 1 - train;
        const validationValue = (validate * rest) / (validate + test);
        const testingValue = (test * rest) / (validate + test);

        this.form.patchValue({
          validate: this.fixValueDigits(testingValue) ?
            this.fixValueDigits(validationValue) || 0.01 : this.fixValueDigits(validationValue - 0.01),
          test: this.fixValueDigits(validationValue) ?
            this.fixValueDigits(testingValue) || 0.01 : this.fixValueDigits(testingValue - 0.01),
        });
      }
      this.emitRange();
    });
  }

  private fixValueDigits(value: number, digits: number = 2) {
    return parseFloat(value.toFixed(digits));
  }

  private emitRange() {
    this.range.emit(this.form.getRawValue());
  }

}
