import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatSliderModule } from '@angular/material/slider';
import {ReactiveFormsModule} from '@angular/forms';

import {DatasetRangeComponent} from './dataset-range/dataset-range.component';

@NgModule({
  declarations: [DatasetRangeComponent],
  exports: [
    DatasetRangeComponent
  ],
  imports: [
    CommonModule,
    MatSliderModule,
    MatButtonModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule {
}
