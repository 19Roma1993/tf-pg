import * as fs from 'fs';
import * as util from 'util';

const nodePlayground = () => {
  console.log('global', global);
  console.log('process', process, process.cwd());

  const writeFile = util.promisify(fs.writeFile);
  const appendFile = util.promisify(fs.appendFile);

  writeFile('notes.txt', 'Who am I, darling, to you?\nWho am I?').then(() => {
    console.log('writeFile success');
    return appendFile('notes.txt', '\nGonna tell you story of mine.');
  }).then(() => {
    console.log('appendFile success');
  }).catch((err: NodeJS.ErrnoException) => {
    throw err;
  });

};

export default nodePlayground;
