import requestPromise from 'request-promise-native';

import { CarEnum, UrlEnum } from '../models/enums';

export const getDataset = () => requestPromise({
  method: 'GET',
  uri: UrlEnum.CARS
});

export const itemClean = (item: any) => item !== null && item !== undefined && item !== '';

export const cleanDataset = (cars: any[]) => {
  return cars.filter((car: any) =>
    itemClean(car[CarEnum.NAME]) &&
    itemClean(car[CarEnum.MILES_PER_GALLON]) &&
    itemClean(car[CarEnum.CYLINDERS]) &&
    itemClean(car[CarEnum.DISPLACEMENT]) &&
    itemClean(car[CarEnum.HORSEPOWER]) &&
    itemClean(car[CarEnum.WEIGHT_IN_LBS]) &&
    itemClean(car[CarEnum.ACCELERATION]) &&
    itemClean(car[CarEnum.YEAR]) &&
    itemClean(car[CarEnum.ORIGIN])
  );
};
