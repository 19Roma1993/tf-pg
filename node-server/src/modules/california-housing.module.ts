import { CSVDataset, Dataset } from '@tensorflow/tfjs-data';
import * as tf from '@tensorflow/tfjs-node';
import { Tensor, Sequential, History } from '@tensorflow/tfjs-node';

import { UrlEnum, HousingEnum } from '../models/enums';

export const getDataset = async () => {
  console.log('tf.memory() before', tf.memory());

  const originalCsvDataset: Dataset<any> = tf.data.csv(UrlEnum.CALIFORNIA_HOUSING_TRAIN, {
    columnConfigs: {
      'median_house_value': {
        isLabel: true,
        required: true
      },
      'total_rooms': {
        isLabel: false,
        required: true
      },
      'population': {
        isLabel: false,
        required: true
      }
    },
    // configuredColumnsOnly: true
  })
    // .take(20)
    .mapAsync(async (value: { [key: string]: any }) => {
      value.ys[HousingEnum.MEDIAN_HOUSE_VALUE] *= 0.001;
      value.xs[HousingEnum.ROOMS_PER_PERSON] = value.xs[HousingEnum.TOTAL_ROOMS] / value.xs[HousingEnum.POPULATION];

      // value.xs[HousingEnum.LONGITUDE] = makeBinaryVector(
      //   value.xs[HousingEnum.LONGITUDE],
      //   -124.3499984741211,
      //   -114.30999755859375
      // );
      // value.xs[HousingEnum.LATITUDE] = makeBinaryVector(
      //   value.xs[HousingEnum.LATITUDE],
      //   32.540000915527344,
      //   41.95000076293945
      // );
      return value;
    })
    .shuffle(2342, '1', false);

  await (await originalCsvDataset.iterator()).resolveFully();

  console.log('originalCsvDataset', originalCsvDataset);
  console.log('tf.memory() after', tf.memory());
  return await originalCsvDataset.toArray();
};
