import express from 'express';
import compression from 'compression';
import * as bodyParser from 'body-parser';

import * as californiaHousing from './controllers/california-housing.controller';
import * as cars from './controllers/cars.controller';

const app = express();

// Express configuration
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req: express.Request, res: express.Response) => {
  res.send('Hallo');
});
app.get('/california-housing', californiaHousing.getData);
app.get('/cars', cars.getData);

export default app;
