import app from './app';
import errorHandler from 'errorhandler';
import nodePlayground from './modules/node-playground';
import tfPlayground from './modules/tf-playground';

if (process.env.NODE_ENV === 'development') {
  app.use(errorHandler());
}

// nodePlayground();
// tfPlayground();

/**
 * Start Express server.
 */
const server = app.listen(app.get('port'), (err: any) => {
  if (!!err === true) {
    console.log('something bad happened', err);
    return;
  }
  console.log(
    '  App is running at http://localhost:%d in %s mode',
    app.get('port'),
    app.get('env')
  );
});

export default server;
