import { Request, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';
import * as util from 'util';
import * as fs from 'fs';

import {cleanDataset, getDataset} from '../modules/cars.module';

export const getData = async (req: Request, res: Response, next: NextFunction) => {

  const readFile = util.promisify(fs.readFile);
  const writeFile = util.promisify(fs.writeFile);

  const sendCarsData = (cars: any) => {
    res.send(cleanDataset(cars));
  };

  readFile('cars.json').then((data: Buffer) => {
    console.log('cars readFile', typeof data);
    const cars = JSON.parse(data as unknown as string);
    sendCarsData(cars);
  }).catch(async (err: any) => {
    console.log('err', err);
    const cars = await getDataset();
    console.log('cars writeFile', typeof cars);
    sendCarsData(JSON.parse(cars));
    await writeFile('cars.json', cars);
  });

};
