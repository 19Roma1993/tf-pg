import { Request, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';
import * as util from 'util';
import * as fs from 'fs';

import { getDataset } from '../modules/california-housing.module';

export const getData = async (req: Request, res: Response, next: NextFunction) => {

  const readFile = util.promisify(fs.readFile);
  const writeFile = util.promisify(fs.writeFile);

  const sendHousingData = (housing: any) => {
    res.send(housing);
  };

  readFile('housing.json').then((data: Buffer) => {
    console.log('readFile', data);
    const housing = JSON.parse(data as unknown as string);
    sendHousingData(housing);
  }).catch(async (err: any) => {
    console.log('err', err);
    const housing = await getDataset();
    sendHousingData(housing);
    await writeFile('housing.json', JSON.stringify(housing));
  });

};
