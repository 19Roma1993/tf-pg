export const getRandomItemsFromArray = (arr: any[], batch: number) => {
  const arrayLength: number = batch < arr.length ? batch : arr.length;
  return Array.from(new Array(arrayLength), (value: any, index: number) => {
    const randomIndex = Math.floor(Math.random() * (arr.length - index));
    const randomElement = arr[randomIndex];
    arr.splice(randomIndex, 1);
    arr.push(randomElement);
    return randomElement;
  });
};

export const makeBinaryVector = (value: number, minValue: number, maxValue: number): number[] => {
  return Array.from(new Array(Math.ceil(maxValue) - Math.floor(minValue)), (v: any, index: number) => {
    return Math.floor(value) - Math.floor(minValue) === index ? 1 : 0;
  });
};
