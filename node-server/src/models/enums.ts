export enum UrlEnum {
  CALIFORNIA_HOUSING_TRAIN = 'https://download.mlcc.google.com/mledu-datasets/california_housing_train.csv',
  BOSTON_HOUSING_TRAIN = 'https://storage.googleapis.com/tfjs-examples/multivariate-linear-regression/data/boston-housing-train.csv',
  CARS = 'https://storage.googleapis.com/tfjs-tutorials/carsData.json'
}

export enum CarEnum {
  NAME = 'Name',
  MILES_PER_GALLON = 'Miles_per_Gallon',
  CYLINDERS = 'Cylinders',
  DISPLACEMENT = 'Displacement',
  HORSEPOWER = 'Horsepower',
  WEIGHT_IN_LBS = 'Weight_in_lbs',
  ACCELERATION = 'Acceleration',
  YEAR = 'Year',
  ORIGIN = 'Origin',
}

export const enum HousingEnum {
  LONGITUDE = 'longitude',
  LATITUDE = 'latitude',
  HOUSING_MEDIAN_AGE = 'housing_median_age',
  TOTAL_ROOMS = 'total_rooms',
  TOTAL_BEDROOMS = 'total_bedrooms',
  POPULATION = 'population',
  HOUSEHOLDS = 'households',
  MEDIAN_INCOME = 'median_income',
  ROOMS_PER_PERSON = 'rooms_per_person',
  MEDIAN_HOUSE_VALUE = 'median_house_value',
}
